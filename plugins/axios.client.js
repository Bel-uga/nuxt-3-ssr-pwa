import axios from "axios";
export default defineNuxtPlugin(nuxtApp => {
    let $axios = axios.create()
    return {
        provide: {
            axios: $axios
        }
    }
})