// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
    app: {
        head: {
            link: [
                { rel: 'stylesheet', href: 'https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css', integrity: 'sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor', crossorigin: 'anonymous' }
            ],
        }
    },
    vite: {
        define: {
            "process.env.DEBUG": false,
        },
    },
    script: [
        {
            src: 'bootstrap/dist/js/bootstrap.bundle.min.js'
        }
    ],
    modules: [
        '@pinia/nuxt',
        '@vueuse/nuxt',
        '@kevinmarrec/nuxt-pwa',

    ],
    runtimeConfig: {
        apiSecret: '',
        public: {
            BACKEND_URL: process.env.BACKEND_URL,
        }
    },
    pwa: {
        workbox: {
            enabled: true
            //Пример сервис воркера
            //templatePath: '@/workers/worker.js'
        },
        manifest: {
            name: 'Nuxt 3 PWA',
            short_name: 'Nuxt 3 PWA',
            background_color: '#111827',
            theme_color: '#3B82F6',
        },
        meta: {
            name: 'Nuxt 3 PWA',
            author: 'Nuxt 3 PWA',
            description: 'Nuxt 3 PWA',
            mobileAppIOS: true,
        },
    },
})
