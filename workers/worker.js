importScripts('<%= options.workboxUrl %>')

self.addEventListener('install', () => self.skipWaiting())
self.addEventListener('activate', () => self.clients.claim())

self.addEventListener('waiting', (event) => {
    showSkipWaitingPrompt(event);
});

const showSkipWaitingPrompt = async (event) => {
    self.addEventListener('controlling', () => {
        window.location.reload();
    });
    const updateAccepted = await promptForUpdate();
    if (updateAccepted) {
        self.messageSkipWaiting();
    }
};

const { registerRoute } = workbox.routing
const { NetworkFirst, StaleWhileRevalidate, CacheFirst } = workbox.strategies
const { CacheableResponsePlugin } = workbox.cacheableResponse
const { ExpirationPlugin } = workbox.expiration

registerRoute(
    ({ request }) => {
        return request.mode === 'navigate'
    },
    new NetworkFirst({
        cacheName: 'pages',
        plugins: [
            new CacheableResponsePlugin({ statuses: [200] })
        ]
    })
)

registerRoute(
    ({ request }) =>
        request.destination === 'manifest' ||
        request.destination === 'style' ||
        request.destination === 'script' ||
        request.destination === 'worker',
    new StaleWhileRevalidate({
        cacheName: 'assets',
        plugins: [
            new CacheableResponsePlugin({ statuses: [200] })
        ]
    })
)

registerRoute(
    ({ request }) => request.destination === 'image',
    new CacheFirst({
        cacheName: 'images',
        plugins: [
            new CacheableResponsePlugin({ statuses: [200] }),
            new ExpirationPlugin({ maxEntries: 50, maxAgeSeconds: 60 * 60 * 24 * 30 })
        ]
    })
)