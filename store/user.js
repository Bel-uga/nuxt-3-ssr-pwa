import { defineStore } from 'pinia'

export const UserStore = defineStore('user-store', () => {
    const user = ref(useLocalStorage('user', {
        ID: 1,
        Email: "rafael.ab@mail.ru"
    }))
    const getUser = () => user
    const setUser = (value) => {
        user.value = value
    }

    return {
        getUser,
        setUser
    }
})
