import { defineStore } from 'pinia'

export const CitiesStore = defineStore({
    id: 'cities-store',
    state: () => {
        return {
            cities: [],
        }
    },
    getters: {
        getCities: state => state.cities,
    },
})