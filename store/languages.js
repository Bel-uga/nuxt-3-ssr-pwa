import { defineStore } from 'pinia'

export const LanguagesStore = defineStore({
    id: 'language-store',
    state: () => {
        return {
            languages: [],
            language: {
                ID: 1,
                Code: "kz",
                Name: "Қазақ",
            },
        }
    },
    actions: {},
    getters: {
        getLanguages: state => state.languages,
        getLanguage: state => state.language,
    },
})